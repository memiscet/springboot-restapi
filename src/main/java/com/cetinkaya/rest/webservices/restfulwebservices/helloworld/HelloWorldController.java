package com.cetinkaya.rest.webservices.restfulwebservices.helloworld;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

//controller
@RestController
public class HelloWorldController {


    //Get
    //URI - /Helllo-world
    //method return - "Hello World"

    @GetMapping(path="/hello-world")
    public String helloWorld(){

        return "HelloWorld";
    }




    @GetMapping(path="/hello-world-bean")
    public HelloWorldBean helloWorldBean(){

        return new HelloWorldBean("Hello World from the bean!");
    }

    @GetMapping(path="/hello-world/path-variable/{name}")
    public HelloWorldBean helloWorldPathVariable(@PathVariable String name){

        return new HelloWorldBean(String.format( "Hello World, %s",name));
    }



}
