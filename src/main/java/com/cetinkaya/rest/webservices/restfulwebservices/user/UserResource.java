package com.cetinkaya.rest.webservices.restfulwebservices.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
public class UserResource {

    @Autowired
private UserDaoService service;

    //GET /users
    //retriveAllUsers
    @GetMapping("/users")
    public List<User> retriveAllUsers(){

return service.findAll();
    }




    //GET /users/{id}

    @GetMapping("/users/{id}")
    public User retriveUser(@PathVariable int id){

return service.findOne( id );
    }

@PostMapping("/users")
public ResponseEntity<Object> createUser(@RequestBody User user){

        User savedUser = service.save( user );
        service.save( user );

        //Created
        // /user/{id}   savedUser.getId()


    URI location = ServletUriComponentsBuilder
            .fromCurrentRequest()
            .path("/{id}")
            .buildAndExpand( savedUser.getId() ).toUri();
   return ResponseEntity.created( location ).build();
}





}
